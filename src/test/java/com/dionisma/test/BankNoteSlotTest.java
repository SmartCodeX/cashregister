package com.dionisma.test;

import com.dionisma.model.BankNoteSlot;
import org.junit.Assert;
import org.junit.Test;


public class BankNoteSlotTest {

    @Test
    public void testAddTotalValue() {
        BankNoteSlot slot = new BankNoteSlot(1);
        Assert.assertEquals(100, slot.add(100));
    }

    @Test
    public void testAddAndSubtract() {
        BankNoteSlot slot = new BankNoteSlot(20);
        slot.add(20);
        slot.subtract(10);
        Assert.assertEquals(10, slot.getQuantity());
    }


    @Test
    public void testHasQuantity() {
        BankNoteSlot slot = new BankNoteSlot(10);
        slot.add(3);
        Assert.assertTrue(slot.hasQuantity(3));
    }

}
