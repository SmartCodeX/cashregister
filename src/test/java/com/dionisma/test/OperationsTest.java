package com.dionisma.test;

import com.dionisma.exceptions.InsufficientFundsException;
import com.dionisma.services.CashRegisterService;
import com.dionisma.services.CashRegisterServiceFactory;
import org.junit.Assert;
import org.junit.Test;

import static com.dionisma.operations.OperationFactory.createOperation;
import static com.dionisma.operations.OperationType.*;


public class OperationsTest {

    CashRegisterService cashRegisterService = CashRegisterServiceFactory.getInstance();

    @Test
    public void testClean() throws Exception {
        cashRegisterService.clean();
        Assert.assertEquals("$0 0 0 0 0 0",
                createOperation(SHOW).execute(cashRegisterService));
    }

    @Test
    public void testPutInitialState() throws Exception {
        cashRegisterService.clean();
        Assert.assertEquals("$68 1 2 3 4 5",
                createOperation(PUT).execute(cashRegisterService, 1, 2, 3, 4, 5));
    }

    @Test
    public void testPut() throws Exception {
        cashRegisterService.clean();
        createOperation(PUT).execute(cashRegisterService, 1, 2, 3, 4, 5);
        Assert.assertEquals("$128 2 4 6 4 10",
                createOperation(PUT).execute(cashRegisterService, 1, 2, 3, 0, 5));
    }

    @Test
    public void testTake() throws Exception {
        cashRegisterService.clean();
        createOperation(PUT).execute(cashRegisterService, 2, 4, 6, 4, 10);
        Assert.assertEquals("$43 1 0 3 4 0",
                createOperation(TAKE).execute(cashRegisterService, 1, 4, 3, 0, 10));
    }

    @Test(expected = InsufficientFundsException.class)
    public void testTakeWhenInsufficientFunds() throws Exception {
        cashRegisterService.clean();
        createOperation(PUT).execute(cashRegisterService, 2, 4, 6, 4, 10);
        Assert.assertEquals("$100 1 4 3 4 5",
                createOperation(TAKE).execute(cashRegisterService, 3, 4, 3, 0, 5));
    }

    @Test
    public void testMakeChangeSuccessful() throws Exception {
        cashRegisterService.clean();
        createOperation(PUT).execute(cashRegisterService, 1, 0, 3, 4, 5);
        CashRegisterService result = cashRegisterService.makeChange(8);
        Assert.assertEquals(8, result.getTotalAmount());
    }

    @Test(expected = InsufficientFundsException.class)
    public void testMakeChangeWhenInsufficientFunds() throws Exception {
        cashRegisterService.clean();
        createOperation(PUT).execute(cashRegisterService, 1, 0, 3, 4, 5);
        cashRegisterService.makeChange(97);
    }

}
