package com.dionisma.test;

import com.dionisma.exceptions.InsufficientFundsException;
import com.dionisma.exceptions.NoChangeException;
import com.dionisma.model.BankNoteSlot;
import com.dionisma.services.CashRegisterService;
import com.dionisma.services.CashRegisterServiceFactory;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CashRegisterServiceTest {

    private static CashRegisterService cashRegisterService = CashRegisterServiceFactory.getInstance();

    @Test
    public void testEmptySlots() {
        cashRegisterService.clean();
        List<BankNoteSlot> slots = cashRegisterService.getSlots();
        Assert.assertNotNull(slots);
        Assert.assertEquals(5, slots.size());
    }

    @Test
    public void testPutIn() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.putIn(1, 2, 3, 4, 5);
        Assert.assertEquals(68, cashRegisterService.getTotalAmount());
    }

    @Test
    public void testTakeOut() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.putIn(1, 2, 3, 4, 5);
        cashRegisterService.takeOut(1, 2, 3, 4, 1);
        Assert.assertEquals(4, cashRegisterService.getTotalAmount());
    }

    @Test
    public void testMakeChange() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.putIn(1, 2, 3, 4, 5); // $20 + 2*$10 + 3*$5 + 4*$2 + 5*$1 = $68
        CashRegisterService result = cashRegisterService.makeChange(9);
        Assert.assertEquals(9, result.getTotalAmount());
        Assert.assertEquals(59, cashRegisterService.getTotalAmount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongPutInput() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.putIn();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongTakeOutInput() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.takeOut();
    }

    @Test(expected = InsufficientFundsException.class)
    public void testMakeChangeInsufficientFunds() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.putIn(1, 2, 3, 4, 5);
        cashRegisterService.makeChange(100);
    }

    //make $8 in change when we have $13 in the cash register
    @Test
    public void testMakeChange8WhenHas13() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.putIn(0, 0, 2, 1, 1);// 2 * 5$, 1 * 2$, 1 * 1$
        Assert.assertEquals(13, cashRegisterService.getTotalAmount());
        CashRegisterService result = cashRegisterService.makeChange(8);
        Assert.assertEquals(8, result.getTotalAmount());
        Assert.assertEquals(5, cashRegisterService.getTotalAmount());
    }

    @Test
    public void testMakeChange8WhenHas13_01() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.putIn(0, 0, 1, 4, 0);// 1 * 5$, 4 * 2$, 1 * 1$
        Assert.assertEquals(13, cashRegisterService.getTotalAmount());
        CashRegisterService result = cashRegisterService.makeChange(8);
        Assert.assertEquals(8, result.getTotalAmount());
        Assert.assertEquals(5, cashRegisterService.getTotalAmount());
    }

    @Test(expected = NoChangeException.class)
    public void tesNoChange8WhenHas13() throws Exception {
        cashRegisterService.clean();
        cashRegisterService.putIn(0, 1, 0, 1, 1); // 1 * 10$, 1 * 2$, 1 * 1$
        Assert.assertEquals(13, cashRegisterService.getTotalAmount());
        cashRegisterService.makeChange(8);
    }

}
