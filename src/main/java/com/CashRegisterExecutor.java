package com;

import com.dionisma.operations.Operation;
import com.dionisma.services.CashRegisterServiceFactory;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

import static com.dionisma.operations.OperationFactory.createOperation;

public class CashRegisterExecutor {

    public static void main(String[] args) {
        do {
            System.out.println("ready");

            Scanner reader = new Scanner(System.in);
            String input = reader.nextLine();
            String[] arguments = input.split(" ");
            String action = arguments[0].toLowerCase();

            try {
                Operation operation = createOperation(action);
                int[] params = Stream.of(Arrays.copyOfRange(arguments, 1, arguments.length))
                        .mapToInt(Integer::parseInt).toArray();
                String result = operation.execute(CashRegisterServiceFactory.getInstance(), params);
                System.out.println(result);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (true);
    }
}
