package com.dionisma.services.impl;

import com.dionisma.exceptions.InsufficientFundsException;
import com.dionisma.exceptions.NoChangeException;
import com.dionisma.model.BankNoteSlot;
import com.dionisma.services.CashRegisterService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Arrays.sort;
import static java.util.stream.Collectors.*;

public class CashRegisterServiceImpl implements CashRegisterService {

    private final List<BankNoteSlot> slots = Stream.of(//in case of Java 9 it'll be List.of
            new BankNoteSlot(20),
            new BankNoteSlot(10),
            new BankNoteSlot(5),
            new BankNoteSlot(2),
            new BankNoteSlot(1))
            .collect(toCollection(ArrayList::new));

    @Override
    public List<BankNoteSlot> getSlots() {
        return this.slots;
    }

    @Override
    public void putIn(int... quantities) {
        if (quantities.length != slots.size()) throw new IllegalArgumentException("Incorrect input values!");
        int i = 0;
        for (BankNoteSlot slot : slots) {
            slot.add(quantities[i++]);
        }
    }

    @Override
    public void takeOut(int... quantities) throws InsufficientFundsException {
        if (quantities.length != slots.size()) throw new IllegalArgumentException("Incorrect input values!");
        checkFunds(quantities);
        int i = 0;
        for (BankNoteSlot slot : slots) {
            slot.subtract(quantities[i++]);
        }
    }

    private void checkFunds(int... quantities) throws InsufficientFundsException {
        int i = 0;
        for (BankNoteSlot bankNoteSlot : slots) {
            if (!bankNoteSlot.hasQuantity(quantities[i++])) throw new InsufficientFundsException();
        }
    }

    @Override
    public CashRegisterService makeChange(int changeAmount) throws NoChangeException, InsufficientFundsException {
        if (changeAmount > getTotalAmount()) throw new InsufficientFundsException();

        CashRegisterService result = new CashRegisterServiceImpl();

        List<Integer> plainArray = new ArrayList<>();
        //convert all slots to plain array
        this.slots.iterator().forEachRemaining(slot ->
                IntStream.range(0, slot.getQuantity()).mapToObj(i -> slot.getValue()).forEach(plainArray::add)
        );

        List<Integer> firstFoundCombination = firstChangeCombination(plainArray.toArray(new Integer[plainArray.size()]), changeAmount);

        firstFoundCombination.stream()
                .collect(groupingBy(i -> i, counting())).forEach((denomination, count) -> {
            //it would be better to replace List of slots with Map
            result.getSlots().stream().filter(slot -> denomination.equals(slot.getValue())).findFirst().ifPresent(slot -> slot.add(count.intValue()));
            slots.stream().filter(slot -> denomination.equals(slot.getValue())).findFirst().ifPresent(slot -> slot.subtract(count.intValue()));
        });
        return result;
    }


    private List<Integer> firstChangeCombination(Integer[] allValues, int target) throws NoChangeException {
        List<Integer> result = new ArrayList<>();
        sort(allValues);
        backtrack(result, allValues, new ArrayList<>(), 0, target);
        if (result.isEmpty()) {
            throw new NoChangeException();
        }
        return result;
    }

    private void backtrack(List<Integer> result, Integer[] allValues, List<Integer> temp, int start, int remain) {
        if (remain < 0) return;
        else if (remain == 0) {
            if (result.isEmpty()) {
                result.addAll(temp);//just first found combination, so it's possible to return all correct combinations
            } else {
                return;
            }
        } else {
            // omit duplicates
            IntStream.range(start, allValues.length).filter(i -> i <= start || !allValues[i].equals(allValues[i - 1])).forEachOrdered(i -> {
                temp.add(allValues[i]);
                backtrack(result, allValues, temp, i + 1, remain - allValues[i]);
                temp.remove(temp.size() - 1);
            });
        }
    }

    @Override
    public int getTotalAmount() {
        return this.slots.stream().mapToInt(BankNoteSlot::getTotalValue).sum();
    }

    @Override
    public void clean() {
        this.slots.forEach((BankNoteSlot b) -> b.setQuantity(0));
    }
}
