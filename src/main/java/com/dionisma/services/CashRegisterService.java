package com.dionisma.services;

import com.dionisma.exceptions.InsufficientFundsException;
import com.dionisma.exceptions.NoChangeException;
import com.dionisma.model.BankNoteSlot;

import java.util.List;

/**
 * The cash register service represents Cash Register machine
 */
public interface CashRegisterService {


    /**
     * Get the set of the cash register slots
     *
     * @return
     */
    List<BankNoteSlot> getSlots();

    /**
     * Put the bills into the cash register slots
     *
     * @param quantities banknotes quantities
     */
    void putIn(int... quantities);

    /**
     * Take the banknotes out from the cash register
     *
     * @param quantities banknotes quantities
     * @throws InsufficientFundsException
     */
    void takeOut(int... quantities) throws InsufficientFundsException;

    /**
     * Make change operation
     *
     * @param changeAmount amount of the requested change
     * @return
     * @throws InsufficientFundsException
     * @throws NoChangeException
     */
    CashRegisterService makeChange(int changeAmount) throws InsufficientFundsException, NoChangeException;

    /**
     * Get total amount
     *
     * @return
     */
    int getTotalAmount();

    /**
     * Clean all slots
     */
    void clean();

}
