package com.dionisma.services;

import com.dionisma.services.impl.CashRegisterServiceImpl;

public final class CashRegisterServiceFactory {

    private static CashRegisterService instance;

    private CashRegisterServiceFactory() {
    }

    public synchronized static CashRegisterService getInstance() {
        if (instance == null) {
            instance = new CashRegisterServiceImpl();
        }

        return instance;
    }

}
