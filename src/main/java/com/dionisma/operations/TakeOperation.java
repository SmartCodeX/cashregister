package com.dionisma.operations;

import com.dionisma.services.CashRegisterService;

public class TakeOperation implements Operation {

    @Override
    public String execute(CashRegisterService cashRegisterService, int... params) throws Exception {

        cashRegisterService.takeOut(params);

        return OperationFactory.createOperation(OperationType.SHOW).execute(cashRegisterService, params);
    }
}
