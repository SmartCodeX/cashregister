package com.dionisma.operations;

import com.dionisma.services.CashRegisterService;

public class ChangeOperation implements Operation {

    @Override
    public String execute(CashRegisterService cashRegisterService, int... params) throws Exception {

        if (params.length > 1) throw new IllegalArgumentException();

        CashRegisterService result = cashRegisterService.makeChange(params[0]);

        return OperationFactory.createOperation(OperationType.SHOW).execute(result, params);
    }

}