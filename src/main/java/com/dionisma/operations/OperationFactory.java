package com.dionisma.operations;

import com.dionisma.exceptions.IncorrectOperationException;

public class OperationFactory {
    public static Operation createOperation(String operationType) throws IncorrectOperationException {
        OperationType type = OperationType.valueOf(operationType.toUpperCase());
        return createOperation(type);

    }

    public static Operation createOperation(OperationType type) throws IncorrectOperationException {
        if (type != null) {
            try {
                return type.getOperationClass().newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new IncorrectOperationException(e);
            }
        } else {
            throw new IncorrectOperationException();
        }
    }

}
