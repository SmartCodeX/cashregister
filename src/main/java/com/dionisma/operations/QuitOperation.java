package com.dionisma.operations;

import com.dionisma.services.CashRegisterService;


public class QuitOperation implements Operation {

    @Override
    public String execute(CashRegisterService cashRegisterService, int... params) {
        System.out.println("Good bye!");
        System.exit(1);
        return null;
    }

}