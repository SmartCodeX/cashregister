package com.dionisma.operations;

import com.dionisma.services.CashRegisterService;

public class ShowOperation implements Operation {

    private static final String $ = "$";
    private static final String SPACE = " ";

    @Override
    public String execute(CashRegisterService cashRegisterService, int... params) {
        StringBuilder sb = new StringBuilder();
        sb.append($);
        sb.append(cashRegisterService.getTotalAmount());
        sb.append(SPACE);
        cashRegisterService.getSlots().forEach(slot -> sb.append(slot.getQuantity()).append(SPACE));
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

}