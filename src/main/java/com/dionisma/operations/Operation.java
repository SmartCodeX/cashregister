package com.dionisma.operations;

import com.dionisma.services.CashRegisterService;

/**
 * Operation is the part of the "command pattern",
 * which helps to execute different cash register operations with the input params
 */
public interface Operation {

    String execute(CashRegisterService cashRegisterService, int... params) throws Exception;

}
