package com.dionisma.operations;

import com.dionisma.services.CashRegisterService;


public class PutOperation implements Operation {

    @Override
    public String execute(CashRegisterService cashRegisterService, int... params) throws Exception {

        cashRegisterService.putIn(params);

        return OperationFactory.createOperation(OperationType.SHOW).execute(cashRegisterService, params);

    }

}