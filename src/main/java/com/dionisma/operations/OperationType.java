package com.dionisma.operations;

/**
 * Cash register operation types
 */
public enum OperationType {

    SHOW(ShowOperation.class),
    PUT(PutOperation.class),
    TAKE(TakeOperation.class),
    CHANGE(ChangeOperation.class),
    QUIT(QuitOperation.class);

    private final Class<? extends Operation> clazz;

    OperationType(Class<? extends Operation> clazz) {
        this.clazz = clazz;
    }

    public Class<? extends Operation> getOperationClass() {
        return clazz;
    }

}
