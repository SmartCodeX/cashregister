package com.dionisma.model;

import java.io.Serializable;

/**
 * BankNoteSlot of the Cash Register machine
 */
public class BankNoteSlot implements Serializable {

    private int value;

    private int quantity = 0;

    public BankNoteSlot(int value, int quantity) {
        this.value = value;
        this.quantity = quantity;
    }

    public BankNoteSlot(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotalValue() {
        return quantity * value;
    }

    public int add(int quantity) {
        this.quantity += quantity;
        return getTotalValue();
    }

    public void subtract(int quantity) {
        this.quantity -= quantity;
        if (this.quantity < 0) this.quantity = 0;
    }

    public boolean hasQuantity(int quantity) {
        return this.quantity >= quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankNoteSlot bankNote = (BankNoteSlot) o;

        return value == bankNote.value && quantity == bankNote.quantity;
    }

    @Override
    public int hashCode() {
        int result = value;
        result = 31 * result + quantity;
        return result;
    }
}
