package com.dionisma.exceptions;

public class InsufficientFundsException extends Exception {

    public InsufficientFundsException() {
        super("Insufficient funds in the cash register!");
    }

}
