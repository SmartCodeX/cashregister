package com.dionisma.exceptions;

public class NoChangeException extends Exception {

    public NoChangeException() {
        super("No change !");
    }
}
