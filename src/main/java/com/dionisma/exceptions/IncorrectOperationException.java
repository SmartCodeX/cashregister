package com.dionisma.exceptions;

public class IncorrectOperationException extends Exception {
    public IncorrectOperationException() {
        super("Incorrect operation!");
    }

    public IncorrectOperationException(Throwable throwable) {
        super("Incorrect operation!", throwable);
    }
}
