# README #

Cash Register

Create a cash register that should be able to accept only $20, $10, $5, $2 and $1 bills. 
Given the charge and amount of money received return the change in each denomination 
that should be given from the cash register.